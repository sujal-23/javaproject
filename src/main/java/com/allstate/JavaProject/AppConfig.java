package com.allstate.javaproject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    public String getDateformat() {
        return dateformat;
    }

    @Value("${myconfig.dateformat}")
        private  String dateformat;

    public  String getTimeZone() {
        return timeZone;
    }

    @Value("${myconfig.timezone}")
    private  String timeZone;

    }

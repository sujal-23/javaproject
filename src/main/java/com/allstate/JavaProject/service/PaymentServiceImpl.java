package com.allstate.javaproject.service;

import com.allstate.javaproject.dao.PaymentDAO;
import com.allstate.javaproject.entities.Payment;
import com.allstate.javaproject.service.errorhandler.PaymentBadRequestException;
import com.allstate.javaproject.service.errorhandler.PaymentNotFoundException;
import com.allstate.javaproject.utility.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService{

    @Autowired
    private PaymentDAO payDao;

    @Autowired
    private ValidationHelper valHelper;

    @Override
    public long rowCount() {
        return payDao.rowCount();
    }

    @Override
    public Payment findById(int id) {
        Payment payment = null;

            if (valHelper.validateIntegerGreaterThanZero(id)) {
                payment = payDao.findById(id);
                if (payment == null) {
                    throw new PaymentNotFoundException("No Payment information found.");
                }
            } else {
                throw new PaymentBadRequestException("Payment id cannot be zero.");
            }

        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        List<Payment> payList = null;
        if(!valHelper.isEmptyString(type)) {
             payList = payDao.findByType(type);
             if(payList.size() <= 0)
                 throw new PaymentNotFoundException("No records found");
        }
        else {
            throw new PaymentBadRequestException("enter valid type, type cannot be blank.");
        }

        return payList;
    }

    @Override
    public List<Payment> findAll() {
        List<Payment> payList = null;
        payList = payDao.findAll();
        if(payList.size() <= 0)
            throw new PaymentNotFoundException("No records found");
        return payList;
    }

    @Override
    public int save(Payment paymentObj) {
            if (paymentObj != null &&
                    valHelper.validateIntegerGreaterThanZero(paymentObj.getId()) &&
                    !valHelper.isEmptyString(paymentObj.getType())
            ) {
                return payDao.save(paymentObj);
            }
        return -1;
    }
}

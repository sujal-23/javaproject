package com.allstate.javaproject.service.errorhandler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.InvalidPropertiesFormatException;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PaymentBadRequestException extends RuntimeException {

    public PaymentBadRequestException(String message)
    {
        super(message);
    }
}

package com.allstate.javaproject.service;
import com.allstate.javaproject.entities.Payment;
import java.util.List;

public interface PaymentService {

    long rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    List<Payment> findAll();
    int save(Payment paymentObj);

}

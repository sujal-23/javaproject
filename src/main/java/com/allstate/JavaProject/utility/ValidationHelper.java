package com.allstate.javaproject.utility;

import com.allstate.javaproject.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.function.IntPredicate;

@Component
public class ValidationHelper {

    public  ValidationHelper()
    {

    }

    @Autowired
    private AppConfig appConfig;

    public  boolean isEmptyString(String stringVal) {
        if(stringVal != null && !stringVal.trim().isEmpty())
            return false;
        return true;
    }

    public  boolean validateIntegerGreaterThanZero(int number) {
        IntPredicate pred = (num) -> num > 0 ;
        return pred.test(number);
    }

    public  String convertDateToString(Date todayDate)
    {
        DateFormat todayDateFormat = new SimpleDateFormat(appConfig.getDateformat());
        todayDateFormat.setTimeZone(TimeZone.getTimeZone(appConfig.getTimeZone()));
        todayDateFormat.setLenient(false);
        return todayDateFormat.format(todayDate);
    }

    public  Date convertStringToDate(String todayDateString)
    {
        DateFormat todayDateFormat = new SimpleDateFormat(appConfig.getDateformat());
        todayDateFormat.setLenient(false);
        todayDateFormat.setTimeZone(TimeZone.getTimeZone(appConfig.getTimeZone()));
        ParsePosition pp1 = new ParsePosition(0);
        return todayDateFormat.parse(todayDateString, pp1);
    }
}

package com.allstate.javaproject.utility;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class DateFormatHandler extends StdDeserializer<Date> {

    @Autowired
    ValidationHelper val;

    public DateFormatHandler() {
        this(null);
    }
    public DateFormatHandler(Class<?> dt) {
        super(dt);
    }

    @Override
    public Date deserialize(JsonParser jsonP, DeserializationContext context) throws IOException
    {
        String actualDate = jsonP.getText();
        System.out.println(actualDate);
        try
        {
            System.out.println(val.convertStringToDate(actualDate));
            return val.convertStringToDate(actualDate);
        }
        catch(Exception e) {
            return null;
        }
    }

}

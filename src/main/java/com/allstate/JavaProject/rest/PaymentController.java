package com.allstate.javaproject.rest;

import com.allstate.javaproject.entities.Payment;
import com.allstate.javaproject.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class PaymentController {
    @Autowired
    private PaymentService service;

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public  String getStatus()
    {
        return "Payment Rest Api is Running!!!";

    }

    @RequestMapping(value = "/totalpayments", method = RequestMethod.GET)
    public  long getTotal()
    {
        return service.rowCount();
    }

    @RequestMapping(value = "/payment/{id}", method = RequestMethod.GET)
    public Payment findPaymentByid(@PathVariable("id") int id)
    {
        return service.findById(id);
    }

    @RequestMapping(value = "/paymentByType/{type}", method = RequestMethod.GET)
    public List<Payment> findPaymentByType(@PathVariable("type") String type)
    {
        return service.findByType(type);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public int save(@RequestBody Payment emp)
    {
       return service.save(emp);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Payment> findAll()
    {
        return service.findAll();
    }

}

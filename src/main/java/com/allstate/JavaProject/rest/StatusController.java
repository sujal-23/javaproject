package com.allstate.javaproject.rest;

import com.allstate.javaproject.AppConfig;
import com.allstate.javaproject.utility.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/api")
public class StatusController {
    @Autowired
    private AppConfig app;

    @Autowired
    private ValidationHelper val;

@RequestMapping(value = "/stat", method = RequestMethod.GET)
    public String getStatus()
    {
        System.out.println(app.getDateformat());
        System.out.println(val.convertDateToString(new Date()));
        System.out.println(val.convertStringToDate("12/11/2020 13:25:25"));

        return "Good to Start Java Spring Boot Assignment App";
    }

}

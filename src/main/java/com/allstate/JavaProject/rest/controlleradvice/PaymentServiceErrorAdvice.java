package com.allstate.javaproject.rest.controlleradvice;

import com.allstate.javaproject.entities.Payment;
import com.allstate.javaproject.service.errorhandler.PaymentBadRequestException;
import com.allstate.javaproject.service.errorhandler.PaymentNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import  static  org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@ControllerAdvice
public class PaymentServiceErrorAdvice {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = PaymentNotFoundException.class)
    public ResponseEntity<Object> exception(PaymentNotFoundException e){
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }
    /*public void handle(PaymentNotFoundException e) {
        System.out.println(55555);
    }*/

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler({PaymentBadRequestException.class})
    public ResponseEntity<Object> exception(PaymentBadRequestException e) {
        return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
    }
}

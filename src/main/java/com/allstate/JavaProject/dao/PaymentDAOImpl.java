package com.allstate.javaproject.dao;

import com.allstate.javaproject.entities.Payment;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentDAOImpl implements  PaymentDAO{
    @Autowired
    private MongoTemplate mongpTpl;

    @Override
    public long rowCount() {
        Query query = new Query();
        long rowCount = mongpTpl.count(query, Payment.class);
        return rowCount;
    }

    @Override
    public List<Payment> findAll() {
        return mongpTpl.findAll(Payment.class);
    }

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment paymentObj =  mongpTpl.findOne(query, Payment.class);
        return   paymentObj;
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment> paymentList =mongpTpl.find(query, Payment.class);
        return  paymentList;
    }

    @Override
    public int save(Payment paymentObj) {
       mongpTpl.save(paymentObj);
       return paymentObj.getId();
    }
}

package com.allstate.javaproject.dao;


import com.allstate.javaproject.entities.Payment;

import java.util.List;

public interface PaymentDAO {
    long rowCount();
    List<Payment> findAll();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment paymentObj);
}

package com.allstate.javaproject.service;


import com.allstate.javaproject.entities.Payment;
import com.allstate.javaproject.utility.ValidationHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class PaymentServiceTest {

    @Mock
    private PaymentService payService;

    @Mock
    private ValidationHelper val;

    @Mock
    private Payment payment;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void rowCount_service_success() {
        doReturn(3L).when(payService).rowCount();
        assertEquals(3L, payService.rowCount());
    }

    @Test
    void find_row_by_id_success(){

        Payment  paymentObj = new Payment(21, new Date(), "test Amount", 100.44, 273);
        doReturn(true).when(val).validateIntegerGreaterThanZero(paymentObj.getId());
        doReturn(paymentObj).when(payService).findById(21);
        assertTrue(val.validateIntegerGreaterThanZero(paymentObj.getId()));
        assertEquals(paymentObj, payService.findById(21));
    }

    @Test
    void find_row_by_id_no_result_found(){
        doReturn(false).when(val).validateIntegerGreaterThanZero(0);
        doReturn(new Payment()).when(payService).findById(0);
        assertTrue(!val.validateIntegerGreaterThanZero(0));
        assertEquals(null, payService.findById(22));
    }

    @Test
    void find_row_by_type_success(){
        doReturn(new Date()).when(val).convertStringToDate("12/11/2020 14:42:13");
        Payment  paymentObj = new Payment(21, val.convertStringToDate("12/11/2020 14:42:13"), "test Amount", 100.44, 273);
        Payment  paymentObj1 = new Payment(33, new Date(), "test Amount", 200.44, 203);
        List<Payment> lstPayment = List.of(paymentObj,paymentObj1);
        doReturn(false).when(val).isEmptyString("test Amount");
        doReturn(lstPayment).when(payService).findByType("test Amount");
        assertTrue(!val.isEmptyString("test Amount"));
        assertEquals(lstPayment, payService.findByType("test Amount"));
        assertEquals(2, lstPayment.size());
        assertEquals(paymentObj , lstPayment.get(0));
    }

    @Test
    void find_row_by_type_no_result(){
        doReturn(true).when(val).isEmptyString(null);
        doReturn(new ArrayList<>()).when(payService).findByType(null);
        assertTrue(val.isEmptyString(null));
        assertEquals(new ArrayList<>().size(), payService.findByType(null).size());
    }

    @Test
    void save_success()
    {
        when(payService.save(any(Payment.class))).thenReturn(1);
        assertEquals(1, payService.save(payment));
    }

    @Test
    void  save_success_with_date()
    {
        doReturn(new Date()).when(val).convertStringToDate("12/11/2020 14:42:13");
        Payment  paymentObj = new Payment(21, val.convertStringToDate("12/11/2020 14:42:13"), "test Amount", 100.44, 273);
        when(payService.save(paymentObj)).thenReturn(1);
        assertEquals(1, payService.save(paymentObj));
        assertEquals(paymentObj.getPaymentdate(), val.convertStringToDate("12/11/2020 14:42:13"));
    }

    @Test
    void save_not_success()
    {
        Payment  paymentObj = new Payment(0, new Date(), "", 100.44, 273);
        when(payService.save(paymentObj)).thenReturn(-1);
        assertEquals(-1, payService.save(paymentObj));
    }
}

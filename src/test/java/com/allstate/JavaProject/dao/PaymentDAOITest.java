package com.allstate.javaproject.dao;

import com.allstate.javaproject.entities.Payment;
import com.allstate.javaproject.utility.ValidationHelper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PaymentDAOITest {

    @Autowired
    private PaymentDAO dao;

    @Autowired
    MongoTemplate tpl;

    @Test
    public void save_And_findById_Success() {

        Payment paymentObj = new Payment(4, new Date(), "Cheque", 200.44, 23);
        dao.save(paymentObj);
        assertEquals(paymentObj.getId(), dao.findById(4).getId());
    }


    @AfterAll
    public void Cleanup() {
        Payment payment1 = dao.findById(4);
        tpl.remove(payment1);
    }
}

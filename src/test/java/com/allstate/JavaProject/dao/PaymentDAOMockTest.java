package com.allstate.javaproject.dao;

import com.allstate.javaproject.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentDAOMockTest {

    @Mock
    private PaymentDAO dao;

    @Mock
    private Payment payment;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void rowCount_success() {
        doReturn(1L).when(dao).rowCount();
        assertEquals(1L, dao.rowCount());
    }

    @Test
    void find_row_by_id_success(){
        Payment  paymentObj = new Payment(1, new Date(), "Cheque", 200.44, 23);
        doReturn(paymentObj).when(dao).findById(1);
        assertEquals(paymentObj, dao.findById(1));
    }

    @Test
    void find_row_by_type_success(){
        List<Payment> lstPayment = new ArrayList<>();
        Payment  paymentObj = new Payment(1, new Date(), "Cheque", 200.44, 23);
        lstPayment.add(paymentObj);
        doReturn(lstPayment).when(dao).findByType("Cheque");
        assertEquals(lstPayment, dao.findByType("Cheque"));
        verify(dao).findByType("Cheque");
    }

    @Test
    void find_row_by_id_not_matched(){
        doReturn(payment).when(dao).findById(10);
        assertEquals(payment, dao.findById(10));
    }

    @Test
    void save_success()
    {
        when(dao.save(any(Payment.class))).thenReturn(1);
        System.out.println(dao.save(payment));
        assertEquals(1, dao.save(payment));
    }
    
}

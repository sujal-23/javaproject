package com.allstate.javaproject.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.text.spi.DateFormatProvider;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PaymentEntityTest {
    private Payment paymentObj;

    @BeforeEach
    void setUp() {
        paymentObj = new Payment(1, new Date(), "Cheque", 200.44, 23);
    }

    @Test
    void getPaymentDate() {
        paymentObj.setPaymentdate(new Date());
        assertEquals(0,new Date().compareTo(paymentObj.getPaymentdate()));
    }

    @Test
    void getID(){
        assertEquals(1, paymentObj.getId() );
    }

    @Test
    void getType()
    {
        assertEquals("Cheque", paymentObj.getType());
    }

    @Test
    void getAmount()
    {
        assertEquals(200.44, paymentObj.getAmount());
    }

    @Test
    void getCustomerId()
    {
        assertEquals(23, paymentObj.getCustid());
    }

}

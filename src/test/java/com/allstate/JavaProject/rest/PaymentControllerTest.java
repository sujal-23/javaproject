package com.allstate.javaproject.rest;

import com.allstate.javaproject.entities.Payment;
import com.allstate.javaproject.service.PaymentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.function.ServerRequest;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class PaymentControllerTest {

    @LocalServerPort
    int randomServerPort;
@Mock
RestTemplate restTemp;

    @Mock
    private PaymentService service;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void save_success() throws Exception
    {
        Payment  paymentObj = new Payment(21, new Date(), "test Amount", 100.44, 273);
        String paymentJson = "{\"id\":12,\"paymentdate\":\"2020-11-10T21:19:19.901+00:00\",\"type\": \"Cash\",\"amount\":267.4,\"custid\": 2}";

        when(service.save(any(Payment.class))).thenReturn(1);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/save")
                .accept(MediaType.APPLICATION_JSON).content(paymentJson)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(200, response.getStatus());
    }

    @Test
    void getRowCount() throws Exception {
        when(service.rowCount()).thenReturn(1L);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/totalpayments")
                .accept(MediaType.APPLICATION_JSON).content("1")
                .contentType(MediaType.APPLICATION_JSON);
        String expected = "1";
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(200, response.getStatus());
        //JSONAssert.assertEquals(expected, result.getResponse()
               // .getContentAsString(), false);
    }

    @Test
    void findbyid_success() throws URISyntaxException
    {
        String baseUrl1 = "http://localhost:8080" + "/payment/5";
        URI uri = new URI(baseUrl1);
        Payment  paymentObj = new Payment(21, new Date(), "test Amount", 100.44, 273);
        ResponseEntity<Payment> responseEntity = new ResponseEntity<>(paymentObj, HttpStatus.OK);
        when(restTemp.getForEntity(uri, Payment.class)).thenReturn(responseEntity);
    }

    @Test
    void find_By_Type_success() throws URISyntaxException
    {
        String baseUrl1 = "http://localhost:8080" + "/paymentByType/Cheque";
        URI uri = new URI(baseUrl1);
        Payment  paymentObj = new Payment(21, new Date(), "Cheque", 100.44, 273);
        ResponseEntity<Payment> responseEntity = new ResponseEntity<>(paymentObj, HttpStatus.OK);
        when(restTemp.getForEntity(uri, Payment.class)).thenReturn(responseEntity);
        assertEquals(responseEntity.getStatusCodeValue(), 200);
    }

}
